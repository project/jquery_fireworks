Jquery Fireworks
---------------------------

INTRODUCTION
-----------
  Set canvas-based fireworks to any page.

INSTALLATION:
-------------
  1. Install as you would normally install a contributed Drupal module. 
     See: https://www.drupal.org/node/895232 for further information.

REQUIREMENTS
------------
  Nothing special is required.

CONFIGURATION
-------------
  1. Install the module "Jquery Fireworks".
  2. Go to the “Block Layout”. Eg:- Admin Menu >> structure >> block layout
  3. Go to your block region where you want to put it.
  4. Click the "Place block" button and in the modal dialog click the 
     "Place block" button next to "Canvas based fireworks".
  5. Click on the Save block button.

UNINSTALLATION
--------------
  1. Uninstall as you would normally Uninstall a contributed Drupal module. 
     See: https://www.drupal.org/docs/user_guide/en/config-uninstall.html 
     for further information.
