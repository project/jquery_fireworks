<?php

namespace Drupal\jquery_fireworks\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Jquery Fireworks' Block.
 *
 * @Block(
 *   id = "jquery_fireworks_block",
 *   subject = @Translation("Fireworks Block"),
 *   admin_label = @Translation("Canvas based fireworks")
 * )
 */
class JqueryFireworksBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $markup = '';
    $output = [
      '#type' => 'markup',
      '#markup' => $markup,
      '#attached' => [
        'library' => [
          'jquery_fireworks/fireworksjs',
        ],
      ],
    ];
    return $output;
  }

}
